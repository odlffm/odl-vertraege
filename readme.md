# Vereinbarungen zur Regelung von Nutzung, Schenkung und Leihgabe von Testgeräten
basierend auf den Überlegungen für das Open Device Lab Frankfurt am Main.

## Regelungsbereiche
- Beteiligte (das ODL hat zwei Rechtsverhältnisse: von wem kommen die Geräte und werden sie auf Dauer oder zeitlich beschränkt überlassen?; wer darf die Geräte zu Testzwecken nutzen?)
- Eigentum an Geräten
- Zustand der Geräte (Hard- & Software, Daten), wobei sich die zu lösende Fragen sowohl auf den Eigentümer des Geräts als auch auf den Nutzer zu Testzwecken bezieht
- Nutzung (Umfang & Einschränkung sowohl beim ODL als auch beim Nutzer zu Testzwecken, Freistellung von Ansprüchen Dritter, die gegen das ODL geltend gemacht werden, Protokollierung der Nutzungszeit und des Datenverkehrs bei Nutzung zu Testzwecken)
- Daten (was passiert mit den sich auf dem Gerät befindenden Daten und wer trägt dafür Verantwortung)
- Haftung (Geräte, Daten, Personen), des ODL gegenüber evtl. Eigentümer des Geräts und des Testnutzers ggü. ODL
- Beendigung der Übergabe an das ODL bei nur zeitweiser Überlassung und der Testnutzung

## Autoren
Tom Arnold, Oliver Beckenhaub - http://www.odlffm.de
RA Veit Reichert - http://www.akvr.de

Die Texte und Formulierungen sind abstrakt formuliert und können als Anregung für Regelungen anderer Open Device Labs in Deutschland verwendet werden.
Für die Richtigkeit und Vollständigkeit übernehmen wir keine Gewähr und/oder Haftung. Die Liste und die Texte sind nicht abschließend und stellen keine juristische Beratung dar. Bitte wenden Sie sich zur Beratung im Einzelfall an einen Rechtsanwalt.