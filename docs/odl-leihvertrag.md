# Leihvertrag

zwischen

Name, Firma und Vertreter, Adresse
- Verleiher -

und

{Name/Firmierung des ODLs oder Firma}
- {Leihnehmer} -


über folgende Geräte:
.................
.................
.................
.................
.................


Gesamtanzahl:  .................  zu folgenden Vertragsbedingungen:

## Präambel
Der Verleiher überlässt dem Entleiher unentgeltlich ein technisches Gerät zum Gebrauch, damit dieser ihm auf diese Art und Weise überlassene Geräte Dritten wie z.B. Webdesignern und Webentwicklern zu Testzwecken zur Verfügung stellen kann.

## § 1 Leihgegenstand und Vertragszweck
(1) Der Verleiher stellt dem Entleiher die oben aufgelisteten Geräte kostenfrei zur Verfügung.

(2) Der Entleiher unterliegt keiner Nutzungsbeschränkung.

(3) Der Entleiher ist berechtigt, sofern noch nicht durchgeführt, jedes Gerät auf Werkseinstellungen zurückzusetzen. Für eventuelle Verluste von Daten des Verleihers oder Dritten übernimmt der Entleiher keine Haftung.

## § 2 Leihzeit und Rückgabe
(1) Die Leihzeit beginnt mit Überlassung der Geräte und endet mit entsprechender Rückgabe.

(2) Der Entleiher ist berechtigt, die Geräte auf Werkseinstellungen zurückzusetzen, sofern möglich. Der Entleiher ist nicht verpflichtet, Modifikationen, Upgrades und/oder Updates rückgängig zu machen. Die Parteien sind sich bewusst, dass die Hersteller von Betriebssystemen und/oder (werkseitig eingesetzter) Software teilweise nur jeweils die aktuellen Versionen bereitstellen.

## § 3 Haftung und Schadensersatz
(1) Der Entleiher haftet bei Vorsatz und grober Fahrlässigkeit, auch seiner Erfüllungsgehilfen, nach den gesetzlichen Bestimmungen. Das gleiche gilt bei fahrlässig verursachten Schäden aus der Verletzung des Lebens, des Körpers oder der Gesundheit. Der Entleiher und seine Erfüllungsgehilfen haften bei fahrlässig verursachten Sach- und Vermögensschäden nur bei der Verletzung einer wesentlichen Vertragspflicht, jedoch der Höhe nach beschränkt auf die bei Vertragsschluss vorhersehbaren und vertragstypischen Schäden. Eine wesentliche Vertragspflicht ist eine solche, deren Erfüllung den Vertrag prägt und auf die der Verleiher vertrauen darf.

(2) Sofern Ansprüche des Entleihers gegen Dritte wegen Verschlechterung, Verlust oder Untergang eines Gerätes bestehen, ist der Entleiher berechtigt, solche Ersatzansprüche zur Abgeltung der gegen ihn gemachten Ersatzansprüche an den Verleiher abzutreten.

(3) Die Gefahr für Verschlechterung, Verlust oder Untergang eines Gerätes durch Zufall trägt der Verleiher.

## § 4 Kündigung
(1) Der Leihvertrag kann jederzeit mit einer Frist von 3 Monaten zum Ende eines Kalendermonats gekündigt werden. Teilkündigungen bezogen auf einzelne Geräte sind möglich.

(2) Die Kündigung hat schriftlich zu erfolgen.

## § 5 Schlussbestimmungen
(1) Mündliche Nebenabreden bestehen nicht. Änderungen oder Ergänzungen dieses Vertrags bedürfen der Schriftform. Dies gilt auch für den Verzicht auf dieses Schriftformerfordernis.

(2) Gerichtsstand für sämtliche Rechtsstreitigkeiten aus oder im Zusammenhang mit diesem Vertrag ist Frankfurt am Main.

{Ort}, den  ...................

..................................
(Verleiher)

..................................
({Leihnehmer})