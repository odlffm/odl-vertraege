# Nutzungsvereinbarung

{Name/Firmierung des ODLs oder Firma}
 - {Bereitsteller} -

und

Name, Firma und Vertreter, Adresse
 - Nutzer -

über folgende Geräte:
.................
.................
.................
.................
.................
.................

Gesamtanzahl:  .................  zu folgenden Vertragsbedingungen:

## Präambel
{Bereitsteller} verfügt über einen Pool an technischen Geräten, die Dritten, insbesondere Webentwicklern und Webdesignern, zu Testzwecken überlassen werden.

## § 1 Leihgegenstand und Vertragszweck
(1) {Bereitsteller} stellt dem Entleiher die oben aufgelisteten Geräte kostenfrei zur Verfügung.

(2) Die Überlassung erfolgt ausschließlich zu Testzwecken innerhalb der Räumlichkeiten der {Bereitsteller} zu üblichen Geschäftszeiten. Eine Überlassung an Dritte ist nicht gestattet.

(3) {Bereitsteller} ist berechtigt, sofern noch nicht durchgeführt, jedes Gerät vor Überlassung und nach Rückgabe auf Werkseinstellungen zurückzusetzen.

## § 2 Nutzung und Rückgabe
(1) Die Nutzung beginnt mit Überlassung des Gerätes und endet mit Rückgabe. Betriebskosten sind vom Nutzer zu tragen.

(2) Der Nutzer ist verpflichtet, bei der Nutzung gesetzliche Vorgaben zu beachten und keine Rechte Dritter zu verletzen.
Der Nutzer stellt {Bereitsteller} von sämtlichen Schadensersatzansprüchen Dritter im Zusammenhang mit seiner Nutzung frei.

(3) Der Nutzer hat eventuelle Daten und von ihm installierte Software selbst zu sichern und vor Rückgabe zu löschen. Der Nutzer hat das Gerät in dem Zustand zurückzugeben, in dem er es erhalten hat. Für eventuelle Verluste von Daten oder Software des Nutzers oder Dritten übernimmt {Bereitsteller} keine Haftung.

## § 3 Haftung
(1) {Bereitsteller} haftet nur für Vorsatz. Dies gilt auch bei arglistigem Verschweigen eines Mangels.

(2) Der Nutzer haftet für jede Veränderung oder Verschlechterung des Gerätes sowie Verlust, Zerstörung oder Vernichtung der Sache.

## § 4 Kündigung
(1) Der Leihvertrag kann jederzeit mit sofortiger Wirkung gekündigt werden. Teilkündigungen bezogen auf einzelne Geräte sind möglich.

(2) Die Kündigung kann mündlich erfolgen.

## § 5 Schlussbestimmungen
(1) Mündliche Nebenabreden bestehen nicht. Änderungen oder Ergänzungen dieses Vertrags bedürfen der Schriftform. Dies gilt auch für den Verzicht auf dieses Schriftformerfordernis.

(2) Gerichtsstand für sämtliche Rechtsstreitigkeiten aus oder im Zusammenhang mit diesem Vertrag ist {ORT}.

{Ort}, den  ....................

........................
({Bereitsteller})

........................
(Nutzer)