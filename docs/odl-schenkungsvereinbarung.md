# Schenkungsvereinbarung

zwischen

Name, Firma und Vertreter, Adresse
- Schenker -

und

{Name/Firmierung des ODLs oder Firma}
- {Beschenkter} -

über folgende Geräte:
.................
.................
.................
.................
.................


Gesamtanzahl:  .................  zu folgenden Vertragsbedingungen:

## Präambel
{Beschenkter} verfügt über einen Pool an technischen Geräten, die Dritten, insbesondere Webentwicklern und Webdesignern, zu Testzwecken überlassen werden.

## § 1 Schenkung
(1) Der Schenker schenkt dem Beschenkten die oben aufgelisteten Geräte.

(2) Der Beschenkte nimmt die Schenkung an.

## § 2 Übergang Eigentum, Besitz, Lasten und Gefahren

Die Parteien sind sich darüber einig, dass das Eigentum und der Besitz an und die Lasten und Gefahren der Sache sofort auf den Beschenkten übergehen.


{Ort}, den  ...................


..................
(Schenker)

....................
({Beschenkter})